const router = require("express").Router();
const config = require("../../../../config/index");
const Invoice = require("../../../../models/invoice");
const User = require("../../../../models/user");
const Activity = require("../../../../middleware/activity");
const WorkFlow = require("../../../../models/workflow");
const InvoiceHistory = require("../../../../models/invoiceshistory");
const transporter = require("../../../../middleware/transporter");
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

//creat Invoice
router.post("/", function (req, res) {
    var _body = req.body;


    WorkFlow.findAll({
        order: [['id', 'ASC']],
        include: [{
            model: User,
            attributes: [
                'email',
                'status',
                'id'
            ]
        }]
    }).then(function (worksFlow) {

        if (!worksFlow || !worksFlow.length) {
            return res.json({
                status: false,
                message: "can`t create invoice(1), no workflow list"
            });
        } else {
            var endMsg = ") are deactivated, so this invoice has been assigned to you",
                message = '';

            for (var i = 0; i < worksFlow.length; i++) {
                var nextApprove = worksFlow[i];
                console.log(nextApprove.person.status);
                if (nextApprove.person.status != config.USER_STATUS.ACTIVE) {
                    message += message.length ? "," + nextApprove.person.email : "Prv Step(s) Users(" + nextApprove.person.email;
                    continue;
                }

                _body.status = 'Pending with User(' + nextApprove.person.email + ')';
                // _body.personPending = nextApprove.person.id;
                _body.personId = nextApprove.person.id;
                if(message.length)_body.rejectedMessage = message+endMsg;
                 return Invoice.create(_body).then(function (invoice) {
                    var mailOptions = {
                        from: config.MAIL.LOGIN, // sender address
                        to: nextApprove.person.email, // list of receivers
                        subject: 'Approve/Reject Invoice(' + invoice.id + ")", // Subject line
                        text: 'An Invoice(' + invoice.id + ') is assigned to yuo, please login and view Inbox to approve/reject it.'
                    };

                    return transporter.sendMail(mailOptions, function (error, info) {
                        if (error) {
                            return res.json({
                                status: true,
                                error: error,
                                message: "An Invoice was created, but was unsuccessfully notified"
                            });
                        }
                        try {
                            Activity.set(req, "create Invoice(" + invoice.id + ")");
                            InvoiceHistory.create({
                                invoiceId: invoice.id,
                                personId: req.user.id,
                                status: 'Created by User(' + req.user.email + ') and pending by User(' + nextApprove.person.email + ')'
                            });
                        } catch (E) {
                            res.json({
                                status: false,
                                error: E,
                                message: "can`t create Invoicc(2), no workflow list"
                            });
                        } finally {
                            return res.json({
                                status: true,
                                data: invoice,
                                message: "An Invoice was created"
                            });
                        }


                    });

                }).catch(function (e) {
                    return res.json({
                        status: false,
                        error: e,
                        message: "Invoice have not been created "
                    });
                });
            }

            return res.json({
                status: false,
                message: "Can`t create an Invoice, all users from workflow are deactivated"
            });


        }

    }).catch(function (e) {
        res.json({
            status: false,
            error: e,
            message: "Can`t create Invoice, no workflow list"
        });

    })

});
//update Invoice
router.put("/", function (req, res) {
    var _invoice = {},
        _body = req.body;

    for (var i = 0; i < Invoice._editable.length; i++) {
        if (_body[Invoice._editable[i]] != undefined) {
            if (i == Invoice._editable.length - 1) {

            } else {
                _invoice[Invoice._editable[i]] = _body[Invoice._editable[i]];
            }

        }
    }
    if (!_body.id) {
        return res.json({
            status: false,
            message: "Invoice was unsuccessfully updated"
        });
    } else {
        Invoice.findOne({where: {id: _body.id}}).then(function (invoiceS) {
            if (invoiceS) {
                Invoice.update(_invoice, {
                    where: {
                        id: _body.id
                    }
                }).then(function (user) {
                    var changes = '', explict = ['createdAt', 'updatedAt', 'id'];
                    for (var field in _invoice) {
                        if (invoiceS[field] == _body[field] || explict.indexOf(field) > -1)continue;
                        if (_body[field] || invoiceS[field])changes += invoiceS[field] + "->" + _invoice[field] + ";";
                    }
                    Activity.set(req, "update Invoice(" + invoiceS.id + ")", changes);
                    return res.json({
                        status: true,
                        message: "Invoice was updated"
                    });
                }).catch(function (e) {
                    return res.status(200).json({
                        status: false,
                        message: "can`t update Invoice"
                    });
                })
            } else {
                return res.status(200).json({
                    status: false,
                    message: "can`t update Invoice2"
                });
            }
        }).catch(function (er) {
            return res.status(200).json({
                status: false,
                error: er,
                message: "can`t update Invoice1"
            });
        })
    }
});
//delete Invoice
router.post("/delete", function (req, res) {
    var _body = req.body;
    if (!_body.id) {
        return res.json({
            status: false,
            message: "can`t delete Invoice, missing required data"
        });
    }
    Invoice.destroy({force: true, where: {id: _body.id}}).then(function () {
        Activity.set(req, "delete Invoice(" + _body.id + ")");
        InvoiceHistory.destroy({where: { invoiceId: _body.id}});
        return res.json({
            status: true,
            message: "Invoice was droped"
        });
    }).catch(function (e) {
        return res.status(200).json({
            status: false,
            message: "can`t delete Invoice"
        });
    })
});


module.exports = router;
