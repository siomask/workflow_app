const router = require("express").Router();
const transporter = require("../../../../middleware/transporter");
const config = require("../../../../config/index");
const Activity = require("../../../../middleware/activity");
const User = require("../../../../models/user");
const WorkFlow = require("../../../../models/workflow");
const Invoice = require("../../../../models/invoice");
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const sequelize = require("../../../../middleware/mysql");

router.get("/users", function (req, res) {
    User.findAll({
        where: {
            status: config.USER_STATUS.ACTIVE,
            id: {
                [Op.notIn]: [req.user.id]
            }
        },
        attributes: {exclude: ['id', 'email']},
        order: [['id', 'ASC']]
    }).then(function (users) {
        res.json({
            status: true,
            data: users,
            message: "Users loaded"
        });
    }).catch(function (e) {
        res.json({
            status: false,
            error: e,
            message: "Users not loaded"
        });
    });
});
router.get("/", function (req, res) {

    WorkFlow.findAll({
        /*include: [{
            model: User,
            where: {personPendingId: Sequelize.col('person.id')},
            attributes: [
                'email',
                'id'
            ]
        }],*/
        order: [['id', 'ASC']]
    }).then(function (data) {
        res.json({
            status: true,
            data: data,
            message: "WorkFlow loaded"
        });
    }).catch(function (e) {
        res.json({
            status: false,
            error: e,
            message: "WorkFlow not loaded"
        });
    });
});
router.post("/", function (req, res) {
    if (!req.body.personPendingId) {
        return res.json({
            status: false,
            message: "missing required data"
        });
    }
    req.body.personId = req.body.personPendingId;
    WorkFlow.create(req.body).then(function (work) {
        Activity.set(req, "add user(" + req.body.personPendingId + ") to workflow");
        res.json({
            status: true,
            data: work,
            message: "WorkFlow was created"
        });
    }).catch(function (e) {
        res.json({
            status: false,
            error: e,
            message: "WorkFlow was not created"
        });
    });
});
router.put("/", function (req, res) {
    if (!req.body.personPendingId || !req.body.id) {
        return res.json({
            status: false,
            message: "missing required data"
        });
    }
    WorkFlow.update(req.body, {where: {id: req.body.id}}).then(function (work) {
        Activity.set(req, "update user(" + req.body.personPendingId + ") in workflow(" + req.body.id + ")");
        res.json({
            status: true,
            data: work,
            message: "WorkFlow was updated"
        });
    }).catch(function (e) {
        res.json({
            status: false,
            error: e,
            message: "WorkFlow was not updated"
        });
    });
});
router.post("/delete", function (req, res) {
    if (!req.body.personPendingId || !req.body.id)return res.json({
        status: false,
        message: "missing required data"
    });
    Invoice.findAll({where: {personId: req.body.personPendingId}}).then(function (invoices) {
        if (invoices && invoices.length) {
            return res.json({
                status: false,
                message: "can`t delete user from workflow, it`s seems he has some invoice to approve"
            });
        } else {
            WorkFlow.destroy({force: true, where: {id: req.body.id}}).then(function () {
                Activity.set(req, "delete user(" + req.body.personPendingId + ") from workflow(" + req.body.id + ")");
                return res.json({
                    status: true,
                    message: "User was droped from workflow"
                });
            }).catch(function (e) {
                return res.status(200).json({
                    status: false,
                    message: "can`t update user from workflow"
                });
            })
        }
    }).catch(function (e) {
        return res.json({
            status: false,
            error: e,
            message: "can`t delete user"
        });
    })

});


module.exports = router;
