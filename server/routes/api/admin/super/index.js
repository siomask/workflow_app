const router = require("express").Router();
const transporter = require("../../../../middleware/transporter");

router.post("/statistic", function (req, res) {
    var store = req.sessionStore.length;
    req.sessionStore.length(function (e, data) {
        res.json({
            status: true,
            countLogIn: data,
            message: "statistic was loaded"
        });
    })
});
router.use("/workflow", require("./workflow"));
router.use("/invoice", require("./invoice"));

module.exports = router;
