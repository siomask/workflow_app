const router = require("express").Router();
const config = require("../../../config/index");
var fs = require("fs");
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const User = require("../../../models/user");
const Activity = require("../../../middleware/activity");
const Invoice = require("../../../models/invoice");
const InvoiceHistory = require("../../../models/invoiceshistory");
const WorkFlow = require("../../../models/workflow");
const transporter = require("../../../middleware/transporter");

//get Invoices
router.get("/invoices", function (req, res) {
    var query = {
        order: [['id', 'DESC']],
        limit:100
    };
    if (req.user.role == config.USER_ROLE.SUPER) {
        // query.include= [{
        //     model: InvoiceHistory,
        //     where: {invoiceId: Sequelize.col('invoice.id')},
        //     attributes: [
        //         'message'
        //     ]
        // }]
        // query.include= [{
        //     model: InvoiceHistory,
        //     through: {
        //         attributes: ['message', 'status' ]
        //     }
        // }]
    }

    Invoice.findAll(query).then(function (invoices) {
        var _whe = [];
        for (var i = 0; i < invoices.length; i++)_whe.push(invoices[i].id);
        InvoiceHistory.findAll({where: {invoiceId: {[Op.in]: _whe}}}).then(function (hist) {
            return res.json({
                status: true,
                data: invoices,
                data1: hist,
                message: "Invoices was successfully get"
            });
        }).catch(function (e) {
            res.json({
                status: false,
                data: e,
                message: "InvoiceHistory was unsuccessfully get"
            });
        })


    }).catch(function (e) {
        res.json({
            status: false,
            data: e,
            message: "Invoice was unsuccessfully get"
        });
    })

});
//get Invoices(for approve)
router.get("/invoices/:id", function (req, res) {
    var query = {
        order: [['id', 'DESC']]
    };
    if (req.user.role == config.USER_ROLE.SUPER) {
    } else {
        query.where = {personId: req.user.id};
    }
    Invoice.findAll(query).then(function (invoices) {
        res.json({
            status: true,
            data: invoices,
            message: "Invoices was successfully get"
        });
    }).catch(function (e) {
        res.json({
            status: false,
            data: e,
            message: "Invoice was unsuccessfully get"
        });
    })

});

function checkUserIfNotBan(userId, next) {
    User.find({where: {id: userId}}).then(function (user) {
        next(!user || user.status != config.USER_STATUS.ACTIVE);
    }).catch(function (er) {
        next(er);
    })
}
//update status of Invoice
router.post("/invoices", function (req, res) {
    var _body = req.body,
        currentUser = req.user.id,
        query = {where: {id: _body.id}};
    if (!_body.id) {
        res.json({
            status: false,
            message: "can`t change the status"
        });
    }

    Invoice.find(query).then(function (invoice) {
        if (invoice.personId != currentUser) {
            res.json({
                status: false,
                message: "It`s not your time to update Invoice"
            });
        } else {

            if (_body.status == 1) {//aproved
                WorkFlow.findAll({
                    order: [['id', 'ASC']],
                    include: [{
                        model: User,
                        where: {personPending: Sequelize.col('person.id')},
                        attributes: [
                            'email',
                            'status',
                            'id'
                        ]
                    }]
                }).then(function (worksFlow) {
                    var endMsg = ") are deactivated, so this invoice has been assigned to you",
                        message = '';
                    for (var i = 0; i < worksFlow.length; i++) {
                        if (worksFlow[i].personId == currentUser) {//Prv Step User(Name) is deactivated, so this invoice has been assigned to you.
                            for (var _u = i + 1; _u < worksFlow.length; _u++) {
                                var nextApprove = worksFlow[_u],
                                    mailOptions = {
                                        from: config.MAIL.LOGIN, // sender address
                                        to: nextApprove.person.email, // list of receivers
                                        subject: 'Approve/Reject Invoice(' + _body.id + ")", // Subject line
                                        text: 'An Invoice(' + _body.id + ') is assigned to yuo, please login and view Inbox to approve/reject it.' //, // plaintext body
                                    };

                                if (nextApprove && nextApprove.person && nextApprove.person.status != config.USER_STATUS.ACTIVE) {
                                    message += message.length ? "," + nextApprove.person.email : "Prv Step(s) Users(" + nextApprove.person.email;
                                    continue;
                                }
                                return transporter.sendMail(mailOptions, function (error, info) {
                                    if (error) {
                                        res.json({
                                            status: false,
                                            error: error,
                                            message: "Invoice was unsuccessfully updated"
                                        });
                                    } else {
                                        Invoice.update({
                                            status: 'Pending with User(' + nextApprove.person.email + ')',
                                            personId: nextApprove.person.id,
                                            rejectedMessage: message.length ? message + endMsg : ''
                                        }, query).then(function (users) {
                                            Activity.set(req, "approved invoice(" + _body.id + ")");
                                            InvoiceHistory.create({
                                                message: _body.message,
                                                invoiceId: _body.id,
                                                personId: currentUser,
                                                status: 'Approved by User(' + req.user.email + ') and pend to User(' + nextApprove.person.email + ')'
                                            });
                                            return res.json({
                                                status: true,
                                                message: "An Invoice was approved"
                                            });
                                        }).catch(function (e) {
                                            res.json({
                                                status: false,
                                                error: e,
                                                message: "Invoice was unsuccessfully updated"
                                            });
                                        })
                                    }
                                });
                            }
                            break;
                        }
                    }

                    Invoice.update({
                        status: 'Approved',
                        personId: null,
                        rejectedMessage: message.length ? message + endMsg : ''
                    }, query).then(function (users) {
                        InvoiceHistory.create({
                            message: _body.message,
                            invoiceId: _body.id,
                            personId: currentUser,
                            status: 'Finish Approved by User(' + req.user.email + ')'
                        });
                        res.json({
                            status: true,
                            message: "Invoices was successfully updated"
                        });
                    }).catch(function (e) {
                        res.json({
                            status: false,
                            error: e,
                            message: "Invoice was unsuccessfully updated"
                        });
                    })
                }).catch(function (e) {
                    res.json({
                        status: false,
                        error: e,
                        message: "can`t update Invoice 1"
                    });

                })
            } else if (_body.status == 2) {//rejected with reasign
                WorkFlow.findAll({
                    order: [['id', 'ASC']],
                    include: [{
                        model: User,
                        where: {personPending: Sequelize.col('person.id')},
                        attributes: [
                            'email',
                            'status',
                            'id'
                        ]
                    }]
                }).then(function (worksFlow) {
                    var endMsg = ") are deactivated, so this invoice has been assigned to you",
                        message = '';
                    for (var i = 0; i < worksFlow.length; i++) {
                        if (worksFlow[i].personId == currentUser) {
                            for (var _u = i - 1; _u >= 0; _u--) {
                                var nextApprove = worksFlow[_u],
                                    mailOptions = {
                                        from: config.MAIL.LOGIN, // sender address
                                        to: nextApprove.person.email, // list of receivers
                                        subject: 'Approve/Reject Invoice(' + _body.id + ")", // Subject line
                                        text: 'An Invoice(' + _body.id + ')  has been rejected by User(' + req.user.email + ') on ' + i + ' step and assigned to you.' //, // plaintext body
                                    };
                                if (nextApprove && nextApprove.person && nextApprove.person.status != config.USER_STATUS.ACTIVE) {
                                    message += message.length ? "," + nextApprove.person.email : "Prv Step(s) Users(" + nextApprove.person.email;
                                    continue;
                                }
                                return transporter.sendMail(mailOptions, function (error, info) {
                                    if (error) {
                                        res.json({
                                            status: false,
                                            error: error,
                                            message: "Invoice was unsuccessfully updated, user(" + nextApprove.person.email + ") wasn not notified"
                                        });
                                    } else {
                                        Invoice.update({
                                            rejectedMessage: message.length ? message + endMsg : '',
                                            status: 'Pending with User(' + nextApprove.person.email + ')',
                                            personId: nextApprove.person.id
                                        }, query).then(function (users) {
                                            Activity.set(req, "rejected invoice(" + _body.id + ")");
                                            InvoiceHistory.create({
                                                message: _body.message,
                                                invoiceId: _body.id,
                                                personId: currentUser,
                                                status: 'Rejected by User(' + req.user.email + ') and pend to User(' + nextApprove.person.email + ')'
                                            });
                                            return res.json({
                                                status: true,
                                                message: "An Invoice was rejected"
                                            });
                                        }).catch(function (e) {
                                            res.json({
                                                status: false,
                                                error: e,
                                                message: "Invoice was unsuccessfully updated 4"
                                            });
                                        })
                                    }

                                });
                            }
                            break;
                        }
                    }
                    Invoice.update({
                        status: 'Rejected',
                        personId: null,
                        rejectedMessage: ''
                    }, query).then(function (users) {
                        InvoiceHistory.create({
                            message: _body.message,
                            invoiceId: _body.id,
                            personId: currentUser,
                            status: 'Finish Rejected by User(' + req.user.email + ')'
                        });
                        res.json({
                            status: true,
                            message: "Invoices was successfully updated"
                        });
                    }).catch(function (e) {
                        res.json({
                            status: false,
                            error: e,
                            message: "Invoice was unsuccessfully updated 5"
                        });
                    })


                }).catch(function (e) {
                    res.json({
                        status: false,
                        error: e,
                        message: "can`t update Invoice 2"
                    });
                })
            } else if (_body.status == 3) {//rejected
                Invoice.update({
                    status: 'Rejected',
                    personId: null,
                    rejectedMessage: _body.message
                }, query).then(function (users) {
                    InvoiceHistory.create({
                        message: _body.message,
                        invoiceId: _body.id,
                        personId: currentUser,
                        status: 'Finish Rejected by User(' + req.user.email + ')'
                    });
                    res.json({
                        status: true,
                        message: "Invoices was successfully updated"
                    });
                }).catch(function (e) {
                    res.json({
                        status: false,
                        error: e,
                        message: "Invoice was unsuccessfully updated 6"
                    });
                })
            }

            else {
                res.json({
                    status: false,
                    message: "Make your correct choise"
                });
            }

        }
    }).catch(function (e) {
        res.json({
            status: false,
            error: e,
            message: "can`t change the status1"
        });
    })


});


//get user(s)
router.get("/", function (req, res) {
    var query = {
        where: {
            id: {
                [Op.notIn]: [req.user.id]
            }
        },
        attributes: {exclude: User._exclude},
        order: [['id', 'DESC']]
    };
    if (req.user.role == config.USER_ROLE.SUPER) {
        // query.where.role = req.query.role || config.USER_ROLE.ADMIN;
    } else {
        query.where.personId = req.user.id;
    }
    User.findAll(query).then(function (users) {
        res.json({
            status: true,
            data: users,
            message: "Users was successfully get"
        });
    }).catch(function (e) {
        res.json({
            status: false,
            data: e,
            message: "Users was unsuccessfully get"
        });
    })

});
//crete new user
router.post("/", function (req, res) {
    var _user = {},
        _body = req.body;

    if (!_body.email || !_body.password) {
        return res.json({
            status: false,
            message: "missing required data"
        });
    } else {
        config.help.cryptPassword(_body.password, function (er, hash) {
            if (er) {
                return res.json({
                    status: false,
                    message: "can`t register 2"
                });
            } else {
                var psw = _body.password;
                _body.password = hash;
                _body.personId = req.user.id;
                _body.role = config.USER_ROLE.ADMIN;
                User.create(_body).then(function (user) {
                    Activity.set(req, "create user(" + user.email + ")", user.email);
                    var mailOptions = {
                        from: req.user.email, // sender address
                        to: user.email, // list of receivers
                        subject: 'Welcome!', // Subject line
                        text: 'Welcome to our service. Use your email as login and password(' + psw + ')' //, // plaintext body
                    };

                    transporter.sendMail(mailOptions, function (error, info) {
                        return res.json({
                            status: true,
                            data: user,
                            message: "user have been created"
                        });
                    })

                }).catch(function (e) {
                    return res.json({
                        status: false,
                        message: "user have not been created, such user already exist "
                    });
                });
            }
        });
    }
});

router.post("/notify", function (req, res) {
    var _body = req.body;
    if (!_body.fromEmail || !_body.subject || !_body.message || !_body.recievers) {
        res.json({
            status: false,
            message: "something missing"
        });
    } else {
        var query = {
                where: {
                    id: {
                        [Op.notIn]: [req.user.id]
                    }
                },
                attributes: ['email']
            },
            notify = [];
        if (req.user.role == config.USER_ROLE.SUPER) {
            if (_body.users)notify.push(config.USER_ROLE.ADMIN);

        } else {
            query.where.personId = req.user.id;
        }
        query.where.role = {[Op.in]: notify};

        User.findAll(query).then(function (users) {
            var notifiers = '';
            if (users)users.forEach(function (user) {
                if (user && user.email)notifiers += "," + user.email;
            });
            notifiers = notifiers.substr(1);
            if (_body.recievers)notifiers += "," + _body.recievers;
            var
                mailOptions = {
                    from: _body.fromEmail, // sender address
                    to: notifiers, // list of receivers
                    subject: _body.subject, // Subject line
                    text: _body.message //, // plaintext body
                };

            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    console.log(error);
                } else {
                    Activity.set(req, config.ACTIVITY.NOTIFY_USER);
                    console.log('Message sent: ' + info.response);
                }
                return res.json({
                    status: true,
                    message: "user(s) was notified"
                });
            });
        }).catch(function (er) {
            console.log(er);
            res.json({
                status: false,
                message: "something missing"
            });
        })
    }


});
router.post("/activity", function (req, res) {
    Activity.get(req, function (er, ect) {
        if (er) {
            return res.json({
                status: false,
                message: "can`t get activity"
            });
        } else {
            return res.json({
                status: true,
                data: ect,
                message: "activity list"
            });
        }
    })
});
//update user
router.put("/", function (req, res) {
    var _user = {},
        _body = req.body;


    for (var i = 0; i < User._editable.length; i++) {
        if (_body[User._editable[i]] != undefined) {
            if (i == User._editable.length - 1) {

            } else {
                _user[User._editable[i]] = _body[User._editable[i]];
            }

        }
    }
    if (!_body.id) {
        res.json({
            status: false,
            message: "Users was unsuccessfully updated"
        });
    } else {
        User.findOne({where: {id: _body.id}}).then(function (userS) {
            if (userS) {
                var updateUser = function () {
                    var _updUr = function () {
                        User.update(_user, {
                            where: {
                                id: _body.id
                            }
                        }).then(function (user) {
                            var changes = '', explict = ['createdAt', 'updatedAt', 'id', 'password'];
                            for (var field in _user) {
                                if (userS[field] == _body[field] || explict.indexOf(field) > -1) {
                                    if (field == explict[3])changes += field + "->" + _body[field];
                                    continue;
                                }
                                if (_body[field] || userS[field])changes += userS[field] + "->" + _user[field] + ";";
                            }
                            Activity.set(req, "update user(" + userS.email + ")", changes);
                            return res.json({
                                status: true,
                                message: "User was updated"
                            });
                        }).catch(function (e) {
                            return res.status(200).json({
                                status: false,
                                message: "can`t update user, such email already exist"
                            });
                        })
                    }
                    if (_body.password) {
                        config.help.cryptPassword(_body.password, function (er, hash) {
                            if (er) {
                                return res.json({
                                    status: false,
                                    message: "can`t register 2"
                                });
                            } else {
                                _user.password = hash;
                                _updUr();
                            }
                        })
                    } else {
                        _updUr();
                    }
                }
                if (typeof userS.status == 'number' && typeof _body.status == 'number' && _body.status != config.USER_STATUS.ACTIVE) {
                    Invoice.findAll({where: {personId: _body.id}}).then(function (invoices) {
                        if (invoices && invoices.length) {
                            return res.json({
                                status: false,
                                message: "can`t update user, it`s seems he has some invoice to approve"
                            });
                        } else {
                            updateUser();
                        }
                    }).catch(function (e) {
                        return res.json({
                            status: false,
                            error: e,
                            message: "can`t update user"
                        });
                    });
                } else {
                    updateUser();
                }

            } else {
                return res.status(200).json({
                    status: false,
                    message: "can`t update user2"
                });
            }
        }).catch(function (er) {
            console.log(er);
            return res.status(200).json({
                status: false,
                message: "can`t update user1"
            });
        })
    }
});

//delete user
router.delete("/:id", function (req, res) {
    var userId = req.params.id;
    WorkFlow.findAll({where: {personId: userId}}).then(function (workFlow) {
        if (workFlow && workFlow.length) {
            return res.json({
                status: false,
                message: "can`t delete user, he used in workflow"
            });
        } else {
            Invoice.findAll({where: {personId: userId}}).then(function (invoices) {
                if (invoices && invoices.length) {
                    return res.json({
                        status: false,
                        message: "can`t delete user, he has some invoice to approve"
                    });
                } else {
                    User.destroy({force: true, where: {id: userId}}).then(function () {
                        Activity.set(req, "delete user(" + userId + ")");
                        return res.json({
                            status: true,
                            message: "User was droped"
                        });
                    }).catch(function (e) {
                        return res.status(200).json({
                            status: false,
                            message: "can`t update user"
                        });
                    })
                }
            }).catch(function (e) {
                return res.json({
                    status: false,
                    error: e,
                    message: "can`t delete user"
                });
            })

        }
    }).catch(function (e) {
        return res.json({
            status: false,
            error: e,
            message: "can`t delete user2"
        });
    })


});

module.exports = router;
