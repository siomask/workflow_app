const router = require("express").Router();
const sequelize = require("sequelize");
const config = require("../../config/index");
const Activity = require("../../middleware/activity");
var fs = require("fs");

const User = require("../../models/user");

router.get("/", function (req, res) {

    // var id = (req.params.id === 'undefined') ? req.user._id : req.params.id;

    var user = req.user;
    user.password = null;
    user.token = null;
    return res.status(200).json({
        status: true,
        message: "Profile data",
        data: user
    });
    /*
     if(user.role == config.USER_ROLE.SUPER){
     User.findAll({
     attributes: [[sequelize.fn('COUNT', sequelize.col('id')), 'users']]
     }).then(function (e) {
     return res.status(200).json({
     status: true,
     message: "Profile data",
     counts:e[0].dataValues.users,
     data: user
     });
     }).catch(function (er) {
     console.log(er);
     return res.status(200).json({
     status: true,
     message: "Customer list",
     data: user
     });
     });
     }else{
     return res.status(200).json({
     status: true,
     message: "Customer list",
     data: user
     });
     }*/


});
//update user
router.put("/", function (req, res) {
    var _body = req.body,
        _user = {},
        changes = '';
    if (_body.id != req.user.id) {
        return res.status(200).json({
            status: false,
            message: "permission denied"
        });
    }
    ['email', 'firstName', 'lastName'].forEach(function (field, keu) {
        if (_body[field] != req.user[field]) {
            _user[field] = _body[field];
            if (keu < 3)changes += req.user[field] + "->" + _user[field] + ";";
        }
    })
    function update() {
        User.update(_user, {
            where: {
                id: _body.id
            }
        }).then(function (user) {
            Activity.set(req, "update user(" + _body.email + ")", changes);
            return res.json({
                status: true,
                message: "User was updated"
            });
        }).catch(function (e) {
            return res.status(200).json({
                status: false,
                message: "can`t update user"
            });
        })
    }

    if (_user.password) {
        _user.password = config.help.cryptPassword(_user.password, function (e, hash) {
            if (e) {
                return res.status(200).json({
                    status: false,
                    message: "can`t update user psw"
                });
            } else {
                _user.password = hash;
                update();
            }
        })
    } else {
        update();
    }

});


module.exports = router;
