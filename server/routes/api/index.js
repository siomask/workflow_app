const router = require("express").Router();
const country = require("../../models/country");

router.all("/", function (req, res) {
    res.json({
        status: true,
        message: "Info about api",
        data: req.user
    });
});

router.use("/user", require("./user"));
router.use("/customer", require("./customer"));
router.use("/admin", require("./admin/index"));


router.get("/countries", function (req,res) {
    country.findAll({ attributes:   ['iso']}).then(function (list) {
        return res.json({
            status: true,
            data: list.map(function(el){return el.iso}),
            message: "list of countries"
        });
    }).catch(function (er) {
        return res.json({
            status: false,
            error: er,
            message: "no list of countries"
        });
    })
});

module.exports = router;
