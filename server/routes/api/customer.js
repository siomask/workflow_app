const router = require("express").Router();
const sequelize = require("sequelize");
const config = require("../../config/index");
var fs = require("fs");
const Op = sequelize.Op;
const Customer = require("../../models/customer");
const Activity = require("../../middleware/activity");

var checkPermission = function (req, res, next) {
    var id = req.body.id || req.params.id;
    if (!id) {
        return res.json({
            status: false,
            message: "can`t update Customer"
        });
    } else {
        if (req.user.role == config.USER_ROLE.SUPER) {
            next();
        } else {
            Customer.findOne({where: {personId: req.user.id, id: id}}).then(function (user) {
                if (user) {
                    next();
                } else {
                    return res.json({
                        status: false,
                        message: "permission denied"
                    });
                }
            }).catch(function (er) {
                console.log(er);
                return res.json({
                    status: false,
                    message: "permission denied"
                });
            })
        }

    }

}
//update customer
router.put("/", function (req, res) {
    var _body = req.body,
        _user = {};
    if (!_body.id) {
        return res.status(200).json({
            status: false,
            message: "can`t update empty Customer"
        });
    } else {
        checkPermission(req, res, function () {
            Customer.findOne({where:{id: _body.id}}).then(function(_customer){
                Customer._editable.forEach(function (field) {
                    _user[field] = _body[field];
                });

                Customer.update(_user, {
                    where: {
                        id: _body.id
                    }
                }).then(function (user) {
                    var changes ='',explict=['createdAt','updatedAt','id'];
                    for(var field in _user){
                        if(_customer[field] == _body[field]||explict.indexOf(field)>-1)continue;
                        if(_body[field]||_customer[field]  )changes +=_customer[field]+"->"+_body[field]+";";
                    }
                    Activity.set(req,"update customer("+_customer.id+")",changes);
                    return res.json({
                        status: true,
                        message: "Customer was updated"
                    });
                }).catch(function (e) {
                    return res.status(200).json({
                        status: false,
                        message: "can`t update Customer"
                    });
                })
            }).catch(function (e) {
                return res.status(200).json({
                    status: false,
                    message: "can`t update Customer"
                });
            })

        });
    }
});
//crete new customer
router.post("/", function (req, res) {
    var _user = {},
        _body = req.body;
    _body.personId = req.user.id;
    if (req.user.role != config.USER_ROLE.SUPER) _body.role = config.USER_ROLE.USER;
    Customer.create(_body).then(function (user) {
        Activity.set(req,"create customer("+user.id+")");
        return res.json({
            status: true,
            data: user,
            message: "Customer have been created"
        });
    }).catch(function (e) {
        return res.json({
            status: false,
            message: "Customer have not been created, such user already exist "
        });
    });
});
//delete customer
router.delete("/:id", function (req, res) {
    checkPermission(req, res, function () {
        Customer.destroy({force: true, where: {id: req.params.id}}).then(function (user) {
            Activity.set(req,"delete customer("+req.params.id+")");
            return res.json({
                status: true,
                message: "Customer was droped"
            });
        }).catch(function (e) {
            return res.status(200).json({
                status: false,
                message: "can`t update Customer"
            });
        })
    });
});
//get customers
router.get("/", function (req, res) {
    var query = {
        order:[['id','DESC']]
    };
    if (req.user.role == config.USER_ROLE.SUPER) {
    } else {
        query.where={personId : req.user.id};
    }
    Customer.findAll(query).then(function (users) {
        res.json({
            status: true,
            data: users,
            message: "Customers was successfully get"
        });
    }).catch(function (e) {
        res.json({
            status: false,
            data: e,
            message: "Customer was unsuccessfully get"
        });
    })

});
module.exports = router;
