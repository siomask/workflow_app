const path = require("path");
const router = require("express").Router();
const sequelize = require("../middleware/mysql");
const authenticate = require("../middleware/authenticate");
const config = require("../config/index");

const User = require("../models/user");
const Country = require("../models/country");
const Invoice = require("../models/invoice");
const InvoiceHistory = require("../models/invoiceshistory");
const Workflow = require("../models/workflow");
const Activity = require("../models/activity");
const Customer = require("../models/customer");
router.use("/api", authenticate, require("./api/index"));

router.use("/auth", require("./auth/index"));

//---only at start app
router.get("/init", function (req, res) {
    try{
        sequelize
            .authenticate()
            .then(function () {
                Country.sync({}).then().catch(function (e) {console.log("WRONG CREATE Invoice",e);});
                return res.json({
                    status: false,
                    message: "can  table"
                });
                User.sync({}).then(function (e) {

                    Invoice.sync({}).then().catch(function (e) {console.log("WRONG CREATE Invoice",e);});
                    InvoiceHistory.sync({}).then().catch(function (e) {console.log("WRONG CREATE InvoiceHistory",e);});
                    Workflow.sync({}).then().catch(function (e) {console.log("WRONG CREATE Workflow",e);});
                    Activity.sync({}).then().catch(function (e) {console.log("WRONG CREATE Activity",e);});
                    Customer.sync({}).then().catch(function (e) {console.log("WRONG CREATE Customer",e);});
                    config.help.cryptPassword(config.superadmin.password, function (er, hash) {
                        if (er) {
                            return res.json({
                                status: false,
                                message: "can`t register 2"
                            });
                        } else {
                            var _user ={};
                            _user.email = config.superadmin.email;
                            _user.firstName = "Super";
                            _user.lastName = "User";
                            _user.role = config.USER_ROLE.SUPER;
                            _user.password = hash;
                            User.create(_user).then(function (user) {
                                return res.json({
                                    status: true,
                                    message: "user table was   created"
                                });
                            }).catch(function (e) {
                                return res.json({
                                    status: false,
                                    message: "user table was bot created"
                                });
                            });
                        }
                    });

                }).catch(function (er) {
                    console.log(er);
                    return res.json({
                        status: false,
                        message: "can`t register user table"
                    });
                })
            }).catch(function (er) {
            return res.json({
                status: false,
                message: "can`t register user table"
            });
        })
    }catch(e){
        return res.json({
            status: false,
            err: e,
            message: "can`t register 2"
        });
    }

});

module.exports = router;
