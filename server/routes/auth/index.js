const passport = require("passport");
const router = require("express").Router();
var jwt = require("jwt-simple");

var config = require("../../config/index");

const User = require("../../models/user");
const sequelize = require("../../middleware/mysql");
const transporter = require("../../middleware/transporter");
const Activity = require("../../middleware/activity");
 

router.all("/", function (req, res, next) {
    res.json({
        status: true,
        message: "Info about auth."
    });
});

/*router.post("/register", function (req, res, next) {
    var passportCtrl = function () {
            passport.authenticate("local", function (err, user, info) {
                if (err) {
                    return res.json({
                        status: false,
                        data: err,
                        message: info.message || "Email or password is incorrect."
                    });
                } else {
                    if (!user) {
                        return res.json({
                            status: false,
                            message: info.message || "Email or password is incorrect."
                        });
                    }

                    req.login(user, function (err) {
                        if (err) {
                            throw err;
                        }

                        res.json({
                            status: true,
                            message: "Login success.",
                            token: user.token
                        });
                    });
                }


            })(req, res, next);
        },
        createUser = function () {
            sequelize
                .authenticate()
                .then(function () {
                    User.sync({}).then(function (e) {
                        var _user = req.body,
                            psw = _user.password;
                        if ((req.body.email === config.superadmin.email) && (req.body.password === config.superadmin.password)) {
                            if (!_user.firstName)_user.firstName = "Super";
                            if (!_user.lastName)_user.lastName = "User";
                            _user.role = config.USER_ROLE.SUPER;
                        }
                        config.help.cryptPassword(psw, function (er, hash) {
                            if (er) {
                                return res.json({
                                    status: false,
                                    message: "can`t register 2"
                                });
                            } else {
                                _user.password = hash;
                                User.create(_user).then(function (user) {
                                    _user.password = psw;
                                    var mailOptions = {
                                        from: config.MAIL.LOGIN, // sender address
                                        to: _user.email, // list of receivers
                                        subject: 'Registration', // Subject line
                                        text: 'Welcome to our service', //, // plaintext body
                                        html: '<b>Hello world ✔</b>' // You can choose to send an HTML body instead
                                    };
                                    transporter.sendMail(mailOptions, function(error, info){
                                        if(error){
                                            console.log(error);
                                        }else{
                                            console.log('Message sent: ' + info.response);
                                        };
                                    });
                                    passportCtrl();
                                });
                            }
                        });
                    });
                    console.log('Connection has been established successfully.');
                })
                .catch(function (err) {
                    console.error('Unable to connect to the database:', err);
                    return res.json({
                        status: false,
                        message: "can`t register"
                    });
                });
        };

    User.findOne({where: {email: req.body.email}}).then(function (user) {
        if (user) {
            return res.json({
                status: false,
                message: "such user already exist"
            });
        } else {
            createUser();
        }
    }).catch(function (err) {
        console.error('Unable to connect to the database:', err);
        createUser();
    });
    ;

});*/

router.post("/login", function (req, res, next) {
    passport.authenticate("local", function (err, user, info) {
        if (err) {
            return res.json({
                status: false,
                data: err,
                message: info.message || "Email or password is incorrect.",
                unlogout:true
            });
        } else {
            if (!user) {
                return res.json({
                    status: false,
                    message: info.message || "Email or password is incorrect.",
                    unlogout:true
                });
            }

            req.login(user, function (err) {
                if (err) {
                    res.json({
                        status: false,
                        message: "Login unsuccess.",
                        unlogout:true
                    });
                   
                }else{
                    res.json({
                        status: true,
                        message: "Login success.",
                        token: user.token
                    });
                }
                
              
            });
        }


    })(req, res, next);

});

router.post("/logout", function (req, res) {
    if (!req.isAuthenticated()) {
        return res.json({
            status: false,
            unlogout:true,
            message: "You are not logged in."
        });
    }

    req.user.token = "";

    req.user.save().then(function () {
        Activity.set(req,config.ACTIVITY.LOGGED_OUR);
        req.logout();
        req.session.destroy();
        res.json({
            status: true,
            message: "Logout success."
        });
    }).catch(function (e) {
        console.log(e);
        res.json({
            status: false,
            data:e,
            unlogout:true,
            message: "Logout unsuccess."
        });
    });
});
router.get("/resetpassword", function (req, res) {
    try{
        var resetD =  (new Buffer(req.url.split("?")[1],'base64').toString().split("&"));
        User.findOne({
            where:{email:resetD[1].split("=")[1],resetPasswordToken:resetD[0].split("=")[1]}
        }).then(function (user) {
            if(!user){
                return res.status(200).json({
                    status: false,
                    message: "can`t reset psw for this user"
                });
            }else{
                config.help.cryptPassword(resetD[2].split("=")[1], function (er, hash) {
                    if (er) {
                        return res.json({
                            status: false,
                            message: "can`t register 2"
                        });
                    } else {

                        user.password = hash;
                        user.resetPasswordToken = null;
                        user.save().then(function () {
                            // res.sendFile(path.join(__dirname, "../dist/index.html"));
                            Activity.set({user:user},config.ACTIVITY.RESET_PSW);
                            res.redirect('/');
                        }).catch(function (er) {
                            return res.json({
                                status: false,
                                message: "can`t reset psw 2"
                            });
                        })
                    }
                });
            }
        }).catch(function (e) {

        })
    }catch (e){
        return res.status(200).json({
            status: false,
            message: "No such user"
        });
    }


})
router.post("/resetPsw", function (req, res) {
    var _email = req.body.email;
    if(_email){
        User.findOne({
            where:{email:_email}
        }).then(function (user) {
            if(!user){
                return res.status(200).json({
                    status: false,
                    message: "No such user"
                });
            }else{
                user.resetPasswordToken =  Date.now() ;
                user.save().then(function (us) {

                    var newPsw = (Date.now()).toString(32),
                        hasTohash = 'token=' + user.resetPasswordToken+"&email="+_email+"&psw="+ newPsw,
                        link = ('http://' + req.headers.host  + '/auth/resetpassword?'+new Buffer(hasTohash).toString('base64')) ;

                    console.log(hasTohash);
                    var mailOptions = {
                        to: _email,
                        from: config.MAIL.LOGIN,
                        subject: 'Password Reset',
                        text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
                        'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
                        link+ '\n\n' +
                        'If you did not request this, please ignore this email and your password will remain unchanged.\n',
                        html: '<h1>Hello '+user.firstName+'</h1>'+
                        '<p>Please click on the following <a href="'+link+'">link</a> to apply the password <b>"'+newPsw+'"</b></p>'
                    };
                    transporter.sendMail(mailOptions, function(error, info){
                        if(error){
                            console.log(error);
                            return res.status(200).json({
                                status: false,
                                message: "can`t inform via email"
                            });
                        }else{
                            console.log('Message sent: ' + info.response);
                            return res.status(200).json({
                                status: true,
                                message: "New temporary password have been send. Pleas view the mail to apply new one"
                            });
                        };

                    });
                }).catch(function (er) {
                    console.log(er);
                    return res.status(200).json({
                        status: false,
                        message: "can`t reset psw"
                    });
                })
            }

        }).catch(function (er) {
            console.log(er);
            return res.status(200).json({
                status: false,
                message: "No such user"
            });
        });
    }else{
        res.json({
            status: false,
            message: "missed the email"
        });
    }
});

module.exports = router;
