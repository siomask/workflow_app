const act = require("../models/activity");
const config = require("../config/index");
const Sequelize = require("sequelize");
const User = require("../models/user");
function Activity() {

}
Activity.set = function (req, action, changes, next) {
    act.create({
        personId: req.user.id,
        action: action,
        changes: changes
    }).then(function (acs) {
        if(next)next();
    }).catch(function (er) {
        console.log("Wrong create activity",er);
        if(next)next(er);
    })
}
Activity.get = function (req, next) {
    var query ={
        include: [{
            model: User,
            where: { id: Sequelize.col('activity.personId') },
            attributes: {include:['email']}
        }],
        order:[['id','DESC']]
    };
    query.limit = req.body.limit || 120;
    if(req.user.role == config.USER_ROLE.SUPER){
    }else{
        query.where={personId :req.user.id};
    }
    act.findAll(query).then(function (acs) {
        if(next)next(false,acs);
    }).catch(function (er) {
        console.log("Wrong get activity",er);
        if(next)next(er);
    })
}
module.exports = Activity;