const passport = require("passport");

module.exports = function (req, res, next) {
    var token = req.headers["authorization"];

    passport.authenticate("jwt", { session: false }, function (err, user, info) {
        // console.log(err,info);
        if (err) {
            return res.status(401).json({
                status: false,
                message: err,
                unlogout:true
            });
        }

        if (!user) {
            return res.status(200).json({
                status: false,
                message: info.message,
                unlogout:true
            });
        }

        if (!req.isAuthenticated()) {
            return res.status(200).json({
                status: false,
                message: "Not authenticated",
                unlogout:true
            });
        }
        if (user.token !== token) {
            return res.status(200).json({
                status: false,
                message: "Expired token",
                unlogout:true
            });
        }
        next();
    })(req, res, next);
};
