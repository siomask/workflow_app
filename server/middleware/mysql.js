const Sequelize = require('sequelize');
const CONFIG = require('../config');
const sequelize = new Sequelize(CONFIG.MYSQL.database, CONFIG.MYSQL.user, CONFIG.MYSQL.password, {
    host: CONFIG.MYSQL.host,
    dialect: 'mysql',//|'sqlite'|'postgres'|'mssql',

    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }//,

    // SQLite only
    // storage: null,//'path/to/database.sqlite'
});
module.exports = sequelize;