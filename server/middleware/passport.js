const jwt = require("jwt-simple");

const LocalStrategy = require("passport-local").Strategy;
const JwtStrategy = require("passport-jwt").Strategy;
const ExtractJwt = require("passport-jwt").ExtractJwt;

const config = require("../config/index");

const User = require("../models/user");
const Activity = require("./activity");

module.exports = function (passport) {
    passport.serializeUser(function (user, done) {
        done(null, user);
    });

    passport.deserializeUser(function (user, done) {
        User.findOne({where: {id: user.id}}).then(function (user) {
            done(null, user);
        }).catch(function (er) {
            done(er);
        });
    });

    passport.use(new LocalStrategy({
        usernameField: "email",
        passwordField: "password",
        passReqToCallback: true
    }, function (req, email, password, done) {

        User.findOne({where: {email: email}}).then(function (user) {
            if (!user) {
                return done(null, false, {
                    status: false,
                    message: "No user found.",
                    unlogout:true
                });
            }
            if (user.status != config.USER_STATUS.ACTIVE) {
                return done(null, false, {
                    status: false,
                    message: "It`s seem`s like you have permission on our app. Contact with us to resolve this issue",
                    unlogout:true
                });
            }
            config.help.comparePassword(password, user.password, function (er, same) {
                if (!same) {
                    return done(null, false, {
                        status: false,
                        message: "Oops! Wrong password.",
                        unlogout:true
                    });
                } else {
                    user.token = "JWT " + jwt.encode({id: user.id, username: user.username}, config.security.secret);
                    // req.session.save();
                    // console.log("---------PASSPOERT*****",user,req.session);
                    user.save().then(function (user) {
                        Activity.set(req,config.ACTIVITY.LOGGED_IN);
                        done(null, user, {
                            status: true,
                            message: "Login success."
                        });
                    }).catch(function (e) {

                    });
                    done(null, user, {
                        status: false,
                        message: "something went wrong",
                        unlogout:true
                    });
                }

            })


        }).catch(function (e) {
            done(null, user, {
                status: false,
                message: "something went wrong1",
                unlogout:true
            });
            console.log(e);
        });
    }));

    passport.use(new JwtStrategy({
        secretOrKey: config.security.secret,
        jwtFromRequest: ExtractJwt.fromAuthHeader()
    }, function (payload, done) {
        User.findOne({where: {id: payload.id}}).then(function (user) {
            /* if (err) {
             return done(err);
             }*/

            if (!user) {
                return done(null, false, {
                    status: false,
                    message: "Failed to authenticate token.",
                    unlogout:true
                });
            }

            done(null, user, {
                status: true,
                message: "Authenticate success."
            });
        }).catch(function (e) {
            console.log(e);
        });
    }));
};
