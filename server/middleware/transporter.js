/*var http = require('http');
 var nodemailer = require('nodemailer');

 var transporter = function () {

 }
 transporter.sendMail = function (opt, next) {
 var _transporter = nodemailer.createTransport({
 host: 'you local host',
 port: 587,//it`s defalt
 secure: false, // true for 465, false for other ports
 auth: {
 user: "", // generated ethereal user
 pass: ""  // generated ethereal password
 }
 });

 _transporter.sendMail(opt, function (error, info) {
 if (error) {
 next(error,{});
 }else{
 next(null,{info:info});
 }
 });

 }

 module.exports = transporter;*/
var nodemailer = require('nodemailer');
var config = require('../config/index');
var transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: config.MAIL.LOGIN, // Your email id
        pass: config.MAIL.PSW // Your password
    }
});
module.exports = transporter;

