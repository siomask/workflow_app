// process.env.NODE_ENV = 'production';
const fs = require("fs");
const bcrypt = require('bcryptjs');

module.exports = {
    env: process.env.NODE_ENV,
    MYSQL: {
        host: 'localhost',
        user: 'root',
        password: '',
        database: 'mean'
    },
   /* MYSQL: {
        host: 'localhost',
        user: 'root',
        password: '',
        database: 'mean'
    }, */
    port: process.env.PORT || 3008,
    MAIL:{
        LOGIN:'simonpdev@gmail.com',
        PSW:'simonpass',
    },
    security: {
        secret: "t45g3w45r34tw5ye454uhdgdf",
        expiresIn: "24h"
    },
    superadmin: {
        email: "simonpdev@gmail.com",
        password: "1111"
    },
    ACTIVITY:{
        LOGGED_IN:1,
        LOGGED_OUR:2,
        CREATE_USER:3,
        UPDATE_USER:4,
        DELETE_USER:5,
        CREATE_CUSTOMER:6,
        UPDATE_CUSTOMER:7,
        DELETE_CUSTOMER:8,
        NOTIFY_USER:9,
        RESET_PSW:10
    },
    FILE_UPLOAD_EXT: ['.obj', 'image/', '.svg'],
    FILE_UPLOAD_ATTR: ['model[]', 'frames[]', 'alignFrames[]', 'structure', 'preloader[]', 'tooltip[]', 'controls[]', 'svgs[]'],
    FILE_UPLOAD_ACCEC: parseInt("0777", 8),

    help: {
        deleteFolderRecursive: function (path, flag) {
            var _self = this;
            if (fs.existsSync(path)) {
                for (var u = 0, files = fs.readdirSync(path); u < files.length; u++) {
                    var file = files[u],
                        curPath = path + "/" + file;
                    if (fs.lstatSync(curPath).isDirectory()) { // recurse
                        _self.deleteFolderRecursive(curPath, true);
                    } else {
                        fs.unlinkSync(curPath);
                    }
                }
                if (flag)fs.rmdirSync(path);
            }
        },
        cryptPassword: function (password, callback) {
            bcrypt.genSalt(10, function (err, salt) {
                if (err)
                    return callback(err);

                bcrypt.hash(password, salt, function (err, hash) {
                    return callback(err, hash);
                });
            });
        },
        comparePassword: function (plainPass, hashword, callback) {
            bcrypt.compare(plainPass, hashword, function (err, isPasswordMatch) {
                return err == null ?
                    callback(null, isPasswordMatch) :
                    callback(err);
            });
        }
    },
    USER_ROLE: {
        SUPER: 1,
        ADMIN: 2,
        USER: 3
    },
    USER_STATUS: {
        ACTIVE: 2,
        BAN: 1
        
    }
}
;
