const Sequelize =  require('sequelize');
const sequelize = require("../middleware/mysql");
const User = require("./user");

const Invoice = sequelize.define('invoice', {
    titile: {
        type: Sequelize.STRING
    } , 
    description: {
        type: Sequelize.STRING
    } ,

    rejectedMessage: {
        type: Sequelize.STRING
    } ,
    item:{ type: Sequelize.INTEGER},
    personPending:{ type: Sequelize.INTEGER},
    invoiceFrom:{ type: Sequelize.STRING},
    billDueDate:{ type: Sequelize.STRING},
    amountDue:{ type: Sequelize.INTEGER},
    amountPaid:{ type: Sequelize.INTEGER},
    billToClientName:{ type: Sequelize.STRING},
    billToClientPhone:{ type: Sequelize.STRING},
    billToClientEmail:{ type: Sequelize.STRING},
    billToClientAdress:{ type: Sequelize.STRING},
    billToClientCity:{ type: Sequelize.STRING},
    billToClientState:{ type: Sequelize.STRING},
    billToClientCountry:{ type: Sequelize.STRING},
    billToClientZipCode:{ type: Sequelize.STRING},
    status:{ type: Sequelize.STRING}
});
Invoice.belongsTo(User);
Invoice._editable=[
    'titile','item','description','billDueDate','amountDue','amountPaid','invoiceFrom',
    'billToClientCity','billToClientState','billToClientCountry','billToClientZipCode',
    'billToClientName','billToClientPhone','billToClientEmail','billToClientAdress'
];
module.exports = Invoice;