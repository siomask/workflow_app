const Sequelize =  require('sequelize');
const sequelize = require("../middleware/mysql");
const User = require("./user");

const WorkFlow = sequelize.define('workflow', {});
WorkFlow.belongsTo(User);
// WorkFlow.belongsTo(User, {as: 'personPending'});
User.hasOne(WorkFlow, { foreignKey: 'personPendingId' });
module.exports = WorkFlow;