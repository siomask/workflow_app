const Sequelize =  require('sequelize');
const sequelize = require("../middleware/mysql");
const config = require("../config/index");

const User = sequelize.define('persons', {
    firstName: {
        type: Sequelize.STRING
    },
    lastName: {
        type: Sequelize.STRING
    },
    role: {
        type: Sequelize.INTEGER,
        defaultValue:config.USER_ROLE.USER
        
    },
    status: {
        type: Sequelize.INTEGER,
        defaultValue:config.USER_STATUS.ACTIVE
    },
    email:{
        type: Sequelize.STRING,
        unique: 'compositeIndex'
    },
    password:{
        type: Sequelize.STRING
    },
    token:{
        type: Sequelize.STRING
    },
    resetPasswordToken :{
        type: Sequelize.STRING
    }  ,
    adress:{
        type: Sequelize.STRING
    },
    telephone:{
        type: Sequelize.STRING
    }
});
User.belongsTo(User);
User._exclude =['password','resetPasswordToken','token'];
User._editable =['firstName','lastName','status','firstName','lastName','status','email','adress','telephone','password'];
module.exports = User;