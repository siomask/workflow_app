const sequelize = require("../middleware/mysql");
const Sequelize =  require('sequelize');

const Contry = sequelize.define('country', {
    iso:{ type: Sequelize.STRING},
    name:{ type: Sequelize.STRING},
    nicename:{ type: Sequelize.STRING},
    iso3:{ type: Sequelize.STRING},
    numcode:{ type: Sequelize.STRING},
    phonecode:{ type: Sequelize.STRING}
});
module.exports = Contry;