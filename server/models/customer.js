const Sequelize =  require('sequelize');
const sequelize = require("../middleware/mysql");
const User = require("./user");

const Customer = sequelize.define('customer', {
    firstName: {
        type: Sequelize.STRING
    },
    lastName: {
        type: Sequelize.STRING
    },
    status: {
        type: Sequelize.INTEGER,
        defaultValue:0
    },

    birthday:{
        type: Sequelize.DATE
    },
    country:{
        type: Sequelize.STRING
    },
    city:{
        type: Sequelize.STRING
    },
    adress:{
        type: Sequelize.STRING
    },
    telephone:{
        type: Sequelize.STRING
    }
});
Customer.belongsTo(User);
Customer._editable = ['firstName','lastName','city','adress','country'];
module.exports = Customer;