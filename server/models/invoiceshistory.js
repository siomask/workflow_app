const Sequelize =  require('sequelize');
const sequelize = require("../middleware/mysql");
const Invoice = require("./invoice");
const User = require("./user");

const InvoiceHistory = sequelize.define('invoicehistory', {
    message:{ type: Sequelize.STRING},
    status:{ type: Sequelize.STRING}
});
InvoiceHistory.belongsTo(Invoice);
InvoiceHistory.belongsTo(User, {as: 'person'});
module.exports = InvoiceHistory;