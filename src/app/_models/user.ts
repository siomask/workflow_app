﻿export class User {
    id:number;
    email:string;
    role:number = 3;
    status:number;
    password:string;
    firstName:string;
    lastName:string;
}