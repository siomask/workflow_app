﻿import {ViewChild, Component, OnInit, TemplateRef, Inject} from '@angular/core';
import {User} from '../../_models/index';
import {AdminService, ConfigService} from '../../_services/index';
import {Config} from '../../_helpers';
import {UserModal, DialogOverviewExampleDialog, CustomerDialog} from '../../_directives';
import {MatPaginator, MatSort, MatTableDataSource, MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

declare var saveAs:any,alasql:any;

@Component({
    moduleId: module.id,
    templateUrl: 'index.html',
    styleUrls: ['index.scss']
})

export class CustomerListComponent implements OnInit {
    private isDesc:boolean = false;
  
    private isFilnishLoaded:boolean = true;
    private modal:boolean = false;
    private model:User = new User();
    private column:string = 'CategoryName';
    private direction:number;
    @ViewChild("userACtion")
    userACtion:UserModal;

    displayedColumns = ['id',  'firstName', 'lastName', 'city','adress','country','createdAt', 'actions'];
    dataSource:MatTableDataSource<any> = new MatTableDataSource([]);
    @ViewChild(MatPaginator) paginator:MatPaginator;
    @ViewChild(MatSort) sort:MatSort;

    constructor(private adminService:AdminService, private confService:ConfigService, private dialog:MatDialog) {
    }

    ngOnInit() {
        setTimeout(()=> {
            this.refreshData();
        })
    }

    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }

    private refreshData() {
        this.adminService.customers.forEach((el)=> {
            el.actions = 1;
        })
        this.dataSource = new MatTableDataSource(this.adminService.customers);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.isFilnishLoaded = false;
    }

    private  openDialog(text, next):void {
        let data:any = {text: text},
            dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
                width: '250px',
                data: data
            });

        dialogRef.afterClosed().subscribe(result => {
            if (data.isOk)next();
        });
    }

    private  openUserDialog(user:any = {}):void {
        let data:any = {
                model: user, onSuccess: (user, next)=> {
                    this.actionUser(user, next);
                }
            },
            dialogRef = this.dialog.open(CustomerDialog, {
                width: '50%',
                data: data
            });
    }
    private actionUser(user, next) {
        if (user.id) {
            this.adminService.updateCustomer(user)
                .subscribe(
                    data => {
                        next();
                    },
                    error => {
                    });
        } else {
            this.adminService.createCustomer(user)
                .subscribe(
                    data => {

                        if (data.status) {
                            this.adminService.customers.push(data.data);
                            this.refreshData();
                        }
                        next();
                    },
                    error => {
                    });
        }

    }
    private applyFilter(filterValue:string) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
        this.dataSource.filter = filterValue;
    }

    private select(user) {
        this.openUserDialog(user);
    }

    private addNewUser() {
        this.openUserDialog();
    }

    private update(user) {
        this.openDialog("are you sure to update " , ()=> {
            let _user = {
                id: user.id,
                status: user.status == Config.USER_STATUS.ACTIVE ? Config.USER_STATUS.BAN : Config.USER_STATUS.ACTIVE
            };
            this.adminService.updateCustomer(_user).subscribe((data)=> {
                if (data.status) {
                    user.status = _user.status;
                }
            });
        });
    }

    private remove(user) {
        this.openDialog("are you sure to drop " + user.id, ()=> {
            this.adminService.deleteCustomer(user.id).subscribe((data)=> {
                if (data.status) {
                    for (let i = 0; i < this.adminService.customers.length; i++) {
                        if (this.adminService.customers[i].id == user.id) {
                            this.adminService.customers.splice(i, 1);
                            return this.refreshData();
                        }
                    }
                }
            });
        });
    }

    private exportXL() {
        if(!this.adminService.customers)return;
        var data1 = this.adminService.customers;
        var data2 = data1;
        var opts = [{sheetid:'One',header:true},{sheetid:'Two',header:false}];
        var res = alasql('SELECT id as Id,  firstName as FirstName,  lastName as LastName,  city as City,  adress as Adress,  country as Country,  createdAt as Created INTO XLSX("Customers.xlsx",?) FROM ?',
            [opts,[data1,data2]]);
     
    };

    private  _sort(property) {
        this.isDesc = !this.isDesc; //change the direction
        this.column = property;
        this.direction = this.isDesc ? 1 : -1;
    };
}