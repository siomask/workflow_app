﻿import {ViewChild, Component, OnInit, TemplateRef, Inject} from '@angular/core';
import {User} from '../../_models/index';
import {WorkFlowService, ConfigService} from '../../_services/index';
import {Config} from '../../_helpers';
import { DialogOverviewExampleDialog, InvoiceDialog} from '../../_directives';
import {MatPaginator, MatSort, MatTableDataSource, MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {InvoicesService, UserService} from "../../_services/user.service";
import {InboxInvoiceDialog} from "../../_directives/inbox-invoice-dialog/index";

declare var alasql:any;


@Component({
    moduleId: module.id,
    templateUrl: 'index.html',
    styleUrls: ['index.scss']
})

export class InboxComponent implements OnInit {
    private isFilnishLoaded:boolean = true;
    private isDesc:boolean = false;
    private modal:boolean = false;
    private model:User = new User();
    private column:string = 'CategoryName';
    private direction:number;
    private isSuper:boolean = false;

    displayedColumns = [
        'id',  'createdAt','billDueDate','amountDue','amountPaid',
        'invoiceFrom','billToClientName','actions'];
    dataSource:MatTableDataSource<any> = new MatTableDataSource([]);
    @ViewChild(MatPaginator) paginator:MatPaginator;
    @ViewChild(MatSort) sort:MatSort;
    private invoiceList:any = [];

    constructor(private invoiceService:InvoicesService, private adminService:UserService, private confService:ConfigService, private dialog:MatDialog) {
    }

    ngOnInit() {
        this.invoiceService.get('personal').subscribe((invoiceList)=> {
            if (invoiceList.status) {
                this.invoiceList = invoiceList.data;
                this.refreshData();
            }
        })
    }

    private applyFilter(filterValue:string) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
        this.dataSource.filter = filterValue;
    }

    private refreshData() {
        this.invoiceList.forEach((el)=> {
            el.actions = 1;
        })
        this.dataSource = new MatTableDataSource(this.invoiceList);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.isFilnishLoaded = false;

    }

     
   

    private select(data) {
        this.openInvoiceDialog(data);
    }

    private  openInvoiceDialog(invoice:any = {}):void {
        let data:any = {
                model: invoice,
                onSuccess: (invoice, next)=> {
                    this.actionInvoice(invoice, next);
                }
            },
            dialogRef = this.dialog.open(InboxInvoiceDialog, {
                width: '80%',
                data: data
            });
    }

    private actionInvoice(invoice, next) {
            this.invoiceService.approve(invoice)
                .subscribe(
                    data => {
                        next();
                        if(data.status){
                            for (let i = 0; i < this.invoiceList.length; i++) {
                                if (this.invoiceList[i].id == invoice.id) {
                                    this.invoiceList.splice(i, 1);
                                    return this.refreshData();
                                }
                            }
                        }
                    },
                    error => {
                    });

    }




}