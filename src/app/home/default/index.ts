﻿import {ViewChild, Component, OnInit, TemplateRef} from '@angular/core';
import {AdminService, ConfigService} from '../../_services/index';
import {UserService, InvoicesService} from "../../_services/user.service";
import {MatTableDataSource} from '@angular/material';
declare var alertify:any;

@Component({
    moduleId: module.id,
    templateUrl: 'index.html',
    styleUrls: ['index.scss']
})

export class DefaultComponent implements OnInit {
    private data:any = {countLogIn: 0, countInv: 0};
    private isFilnishLoaded:boolean = true;
    displayedColumns = ['id', 'user', 'action', 'changes', 'createdAt'];
    dataSource:MatTableDataSource<any> = new MatTableDataSource([]);

    constructor(private adminService:AdminService, private confService:ConfigService, private userService:UserService, private invoiceService:InvoicesService) {
    }

    ngOnInit() {

        this.loadStatistic();

    }

    private loadStatistic(next:Function = null) {
        this.adminService.request('statistic').subscribe((data)=> {
            if (data.status)this.data.countLogIn = data.countLogIn;

            this.adminService.req({limit: 10}, 'activity').subscribe((res)=> {
                if (res.status) {
                    res.data.map((el)=> {
                        el.action = this.confService.ACTIVITY[el.action] || el.action;
                        el.user = el.person.email;
                    });
                    this.dataSource = new MatTableDataSource(res.data);
                    this.isFilnishLoaded = false;
                    this.invoiceService.get(this.userService.isSuperAdmin() ? null : 'personal').subscribe((invoiceList)=> {
                        if (invoiceList.status) {
                            this.data.countInv = invoiceList.data.length;
                        }
                    })
                }
            })
        })
    }
}