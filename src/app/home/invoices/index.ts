﻿import {ViewChild, Component, OnInit, TemplateRef, Inject} from '@angular/core';
import {User} from '../../_models/index';
import {WorkFlowService, ConfigService} from '../../_services/index';
import {Config} from '../../_helpers';
import { DialogOverviewExampleDialog, InvoiceDialog} from '../../_directives';
import {MatPaginator, MatSort, MatTableDataSource, MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {InvoicesService, UserService} from "../../_services/user.service";

declare var alasql:any;


@Component({
    moduleId: module.id,
    templateUrl: 'index.html',
    styleUrls: ['index.scss']
})

export class InvoiceComponent implements OnInit {
    private isFilnishLoaded:boolean = true;
    private isDesc:boolean = false;
    private modal:boolean = false;
    private model:User = new User();
    private column:string = 'CategoryName';
    private direction:number;
    private isSuper:boolean = false;

    displayedColumns = [
         'item' , 'createdAt','billDueDate','amountDue','amountPaid',
        'description','invoiceFrom','billToClientName','billToClientPhone','billToClientEmail',
        'billToClientAdress','billToClientCity','billToClientState','billToClientCountry','billToClientZipCode',
        'status'];
    dataSource:MatTableDataSource<any> = new MatTableDataSource([]);
    @ViewChild(MatPaginator) paginator:MatPaginator;
    @ViewChild(MatSort) sort:MatSort;
    private invoiceList:any = [];

    constructor(private invoiceService:InvoicesService, private adminService:UserService, private confService:ConfigService, private dialog:MatDialog) {
        this.isSuper = adminService.isSuperAdmin();
        if (this.isSuper) {
            this.displayedColumns.push('actions')
        }
    }

    ngOnInit() {
        this.invoiceService.get().subscribe((invoiceList)=> {
            if (invoiceList.status) {
                this.invoiceList = invoiceList.data;
                if(invoiceList.data1){
                    for(let i=0; i<invoiceList.data.length;i++){
                        let _d =invoiceList.data[i];
                        _d._history ='';
                        _d._reject ='';
                        for(let u=0, itemss=1; u<invoiceList.data1.length;u++){
                            if(_d.id==invoiceList.data1[u].invoiceId ){
                                let _curI = invoiceList.data1.splice(u--,1)[0];
                                 _d._history +="<p>"+(itemss)+")"+(_curI.status||'')+"</p>";
                                _d._reject +="<p>"+(itemss++)+")"+(_curI.status||'')+ (_curI.message? ', <b>Message:</b>'+(_curI.message):'')+"</p>";
                            }
                        }
                    }
                }
                this.refreshData();
            }
        })
    }

    private applyFilter(filterValue:string) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
        this.dataSource.filter = filterValue;
    }

    private refreshData() {
        this.invoiceList.forEach((el)=> {
            el.actions = 1;
        })
        this.dataSource = new MatTableDataSource(this.invoiceList);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.isFilnishLoaded = false;

    }
    private exportXL() {
        if (!this.invoiceList)return;
        var data1 = this.invoiceList;
        var data2 = data1;
        var opts = [{sheetid: 'One', header: true}, {sheetid: 'Two', header: false}];
        var res = alasql('SELECT id as InvoiceNo,  createdAt as Invoice_Date, billDueDate as BillDueDate,  amountDue as AmountDue, description as Description, ' +
            'invoiceFrom as InvoiceFrom ,billToClientName, billToClientPhone,billToClientEmail,billToClientAdress,billToClientCity,' +
            'billToClientState,billToClientCountry, status INTO XLSX("Invoices.xlsx",?) FROM ?',
            [opts, [data1, data2]]);
    };

    private add() {
        this.openInvoiceDialog();
    }

   

    private select(data) {
        this.openInvoiceDialog(data);
    }

    private  openInvoiceDialog(invoice:any = {}):void {
        let data:any = {
                model: invoice,
                onSuccess: (invoice, next)=> {
                    this.actionInvoice(invoice, next);
                }
            },
            dialogRef = this.dialog.open(InvoiceDialog, {
                width: '80%',
                data: data
            });
    }

    private actionInvoice(invoice, next) {
        if (invoice.id) {
            this.invoiceService.update(invoice)
                .subscribe(
                    data => {
                        next();
                    },
                    error => {
                    });
        } else {
            this.invoiceService.create(invoice)
                .subscribe(
                    data => {

                        if (data.status) {
                            this.invoiceList.splice(0,0,data.data);
                            this.refreshData();
                        }
                        next();
                    },
                    error => {
                    });
        }

    }

    private remove( workF) {
        this.openDialog("are you sure to drop invoice (" + (workF.id)+")", ()=> {
            this.invoiceService.delete(workF).subscribe((data)=> {
                if (data.status) {
                    for (let i = 0; i < this.invoiceList.length; i++) {
                        if (this.invoiceList[i].id == workF.id) {
                            this.invoiceList.splice(i, 1);
                            return this.refreshData();
                        }
                    }
                }
            });
        });
    }

    private  openDialog(text, next):void {
        let data:any = {text: text},
            dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
                width: '250px',
                data: data
            });
        dialogRef.afterClosed().subscribe(result => {
            if (data.isOk)next();
        });
    }

}