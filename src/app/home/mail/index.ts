﻿import {ViewChild, Component, OnInit, TemplateRef} from '@angular/core';
import {AdminService, ConfigService} from '../../_services/index';
import {UserService} from "../../_services/user.service";
import {FormGroup,FormBuilder,FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';

@Component({
    moduleId: module.id,
    templateUrl: 'index.html',
    styleUrls: ['index.scss']
})

export class MailComponent implements OnInit {
    private loading:boolean = false;
    private model:any = { message:"Goog news for every one!!!",fromEmail:'simonpdev@gmail.com'};
    private userForm: FormGroup;
    constructor(private formBuilder: FormBuilder,private adminService:AdminService, private confService:ConfigService, private userService:UserService) {
        this.userForm = this.formBuilder.group({
            fromEmail:    new FormControl('', [
                Validators.required,
                Validators.email,
            ]),
            recievers:new FormControl('', [Validators.required ]),
            users:new FormControl('', [   ]),
            subject:new FormControl('', [ Validators.required ]),
            message:new FormControl('', [ Validators.required ])
        });
        this.model.fromEmail = this.userService.USER.email;
    }

    ngOnInit() {

    }


   private notify(){
       if(this.loading || !this.userForm.valid)return;
       this.loading = true;
       this.loading = true;
           this.adminService.req(this.model,'notify')
               .subscribe(
                   data => {
                       this.loading = false;
                   },
                   error => {
                       this.loading = false;
                   });
   }
}