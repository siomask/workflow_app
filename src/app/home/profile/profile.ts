﻿import {Component, OnInit} from '@angular/core';

import {User} from '../../_models/index';
import {UserService,ConfigService} from '../../_services/index';
import {FormGroup,FormBuilder,FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
@Component({
    moduleId: module.id,
    templateUrl: 'profile.html',
    styleUrls: ['index.scss']
})

export class ProfileComponent implements OnInit {
    currentUser:any={};
    private loading: boolean;
    private userForm: FormGroup;
    constructor(  private formBuilder: FormBuilder,private userService:UserService,private confService:ConfigService) {
        this.userForm = this.formBuilder.group({
            email:    new FormControl('', [
                Validators.required,
                Validators.email,
            ]),
            psw:new FormControl('', []),
            firstName:new FormControl('', [ Validators.required ]),
            lastName:new FormControl('', [ Validators.required ])
        });
    }

    ngOnInit() {
        if(!this.userService.USER.id){
            this.userService.getOne().subscribe((data)=> {
                this.currentUser = this.userService.USER;
            });
        }else{
            this.currentUser = this.userService.USER;
        }

    }

    private update() {
        if(!this.userForm.valid)return;
        this.loading = true;
        this.userService.update(this.currentUser).subscribe((data)=> {
            this.currentUser.password = this.loading =  null;
        });
    }

     
}