﻿import {ViewChild, Component, OnInit, TemplateRef, Inject} from '@angular/core';
import {User} from '../../_models/index';
import {WorkFlowService, ConfigService} from '../../_services/index';
import {Config} from '../../_helpers';
import {UserModal, DialogOverviewExampleDialog, WorkFlowDialog} from '../../_directives';
import {MatPaginator, MatSort, MatTableDataSource, MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {AdminService} from "../../_services/user.service";

declare var alasql:any;


@Component({
    moduleId: module.id,
    templateUrl: 'index.html',
    styleUrls: ['index.scss']
})

export class WorkFlowComponent implements OnInit {

    private users:any = [];
    private workFlowList:any = [];
    private displayedColumns = ['step', 'user', 'action'];

    constructor(private workService:WorkFlowService, private adminService:AdminService, private confService:ConfigService, private dialog:MatDialog) {
        this.workService.get().subscribe((workData)=> {
            if (workData.status)this.workFlowList = workData.data;
            // this.workService.getUsers().subscribe((data)=> {
            //     if (data.status)this.users = data.data;
            // })
        })
    }

    ngOnInit() {
    }


    private add() {
        this.openWorkFlowDialog();
    }

    private getUser(id,column) {
        for (let i = 0, _u = this.adminService.users; i <_u.length; i++) {
            if (_u[i].id == id)return _u[i][column]+(column =='email' && _u[i].status == this.confService.USER_STATUS.BAN ?'(Deactivated)':'');
        }
        return id;
    }

    private select(workF) {
        this.openWorkFlowDialog(workF);
    }

    private  openWorkFlowDialog(user:any = {}):void {
        let data:any = {
                model: user,
                users: this.adminService.users.filter((el)=>{
                    for(let i =0;i<this.workFlowList.length;i++)if(this.workFlowList[i].personId ==el.id )return false;
                    return el.status != this.confService.USER_ROLE.BAN }),
                onSuccess: (user, next)=> {
                    this.actionWork(user, next);
                }
            },
            dialogRef = this.dialog.open(WorkFlowDialog, {
                width: '50%',
                data: data
            });
    }

    private actionWork(work, next) {
        if (work.id) {
            this.workService.update(work)
                .subscribe(
                    data => {
                        next();
                    },
                    error => {
                    });
        } else {
            this.workService.create(work)
                .subscribe(
                    data => {

                        if (data.status) {
                            this.workFlowList.push(data.data);
                        }
                        next();
                    },
                    error => {
                    });
        }

    }

    private remove(item, workF) {
        this.openDialog("are you sure to drop user from step " + (item+1), ()=> {
            this.workService.delete(workF).subscribe((data)=> {
                if (data.status) {
                    this.workFlowList.splice(item, 1);
                }
            });
        });
    }

    private  openDialog(text, next):void {
        let data:any = {text: text},
            dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
                width: '250px',
                data: data
            });
        dialogRef.afterClosed().subscribe(result => {
            if (data.isOk)next();
        });
    }

}