﻿import {ViewChild, Component, OnInit, TemplateRef} from '@angular/core';
import {AdminService, ConfigService} from '../../_services/index';
import {UserService} from "../../_services/user.service";
import {MatPaginator, MatSort, MatTableDataSource, MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {Sort} from '@angular/material';
declare var alasql:any;

@Component({
    moduleId: module.id,
    templateUrl: 'index.html',
    styleUrls: ['index.scss']
})

export class ActivityComponent implements OnInit {


    private isFilnishLoaded:boolean = true;
    private activity:Array<any>=[];
    displayedColumns = ['id','user',  'action', 'changes', 'createdAt' ];
    dataSource:MatTableDataSource<any> = new MatTableDataSource([]);
    @ViewChild(MatPaginator) paginator:MatPaginator;
    @ViewChild(MatSort) sort:MatSort;
    constructor(private adminService:AdminService, private confService:ConfigService, private userService:UserService) {
    }

    ngOnInit() {

        this.adminService.req(null,'activity').subscribe((res)=>{
            if(res.status){
                
                res.data.map((el)=>{
                    el.action = this.confService.ACTIVITY[el.action] || el.action;
                    el.user = el.person.email;
                });
                this.activity = res.data;
                this.dataSource = new MatTableDataSource(res.data);
                this.dataSource.paginator = this.paginator;
                this.dataSource.sort = this.sort;
                this.isFilnishLoaded = false;
            }
        })
    }
    private exportXL() {
        var data1 = this.adminService.users;
        var data2 = data1;
        var opts = [{sheetid:'One',header:true},{sheetid:'Two',header:false}];
        var res = alasql('SELECT id as Id,  user as User, action as Action,  changes as Changes, createdAt as Created INTO XLSX("Activity.xlsx",?) FROM ?',
            [opts,[data1,data2]]);

    };
    private applyFilter(filterValue:string) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
        this.dataSource.filter = filterValue;
    }
}