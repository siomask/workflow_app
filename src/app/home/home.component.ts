﻿import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthenticationService, UserService, AdminService} from '../_services/index';
import {ConfigService} from "../_services/config";

@Component({
    moduleId: module.id,
    templateUrl: 'home.component.html',
    styleUrls: ['index.scss']
})

export class HomeComponent implements OnInit {


    constructor(private authService:AuthenticationService,
                private confService:ConfigService,
                private userService:UserService,
                private adminService:AdminService) {
    }

    ngOnInit() {
        this.userService.getOne().subscribe((data)=> {
            this.adminService.getAllCustomer({}).subscribe((data)=> {
                if (data.status)this.adminService.customers = data.data;
                if (this.userService.USER.role == this.confService.USER_ROLE.SUPER){
                    this.adminService.getAll({}).subscribe((data)=> {
                        if (data.status)this.adminService.users = data.data;
                    });
                }
            });



        });
    }


    private logOut() {
        this.authService.logout().subscribe((data:any)=> {
            console.log(data);
        })
    }
}