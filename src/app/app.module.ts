import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule }    from '@angular/forms';
import { HttpModule,BaseRequestOptions } from '@angular/http';


import { AppComponent } from './app.component';
import { routing }        from './app.routing';

import {MatCardModule} from '@angular/material/card';
import {MatGridListModule} from '@angular/material/grid-list';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MatButtonModule, MatCheckboxModule } from '@angular/material';
import {MatIconModule} from '@angular/material/icon';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatInputModule} from '@angular/material/input';
import {MatSortModule} from '@angular/material/sort';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatDialogModule} from '@angular/material/dialog';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSelectModule} from '@angular/material/select';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule,MatAutocompleteModule} from '@angular/material'

import { AlertComponent,UserModal,DialogOverviewExampleDialog,UserDialog,CustomerDialog,WorkFlowDialog } from './_directives/index';
import { AuthGuard,LoginGuard ,AdminGuard,SuperAdminGuard} from './_guards/index';
import { AlertService, AuthenticationService, UserService,AdminService,ConfigService,WorkFlowService,InvoicesService } from './_services/index';
import { HomeComponent,UserListComponent,CustomerListComponent ,ProfileComponent,MailComponent,DefaultComponent,ActivityComponent,WorkFlowComponent } from './home/index';
import { LoginComponent } from './login/index';
import { FilterNamesPipe,OrderrByPipe } from './_pipes';
import {CdkTableModule} from "@angular/cdk/table";
import {InvoiceDialog} from "./_directives/invoice-dialog/index";
import {InvoiceComponent} from "./home/invoices/index";
import {InboxInvoiceDialog} from "./_directives/inbox-invoice-dialog/index";
import {AdminOnlyGuard} from "./_guards/auth.guard";
import {InboxComponent} from "./home/inbox/index";
import {ForgotComponent} from "./resetpsw";



@NgModule({
  declarations: [
    AppComponent,

    AlertComponent,
    HomeComponent,
    ProfileComponent,
    UserListComponent,
    CustomerListComponent,
    LoginComponent,
    MailComponent,
    ActivityComponent,
    DefaultComponent,
    WorkFlowComponent ,
    InvoiceComponent ,
    InboxComponent ,
    ForgotComponent ,

    UserModal,
    DialogOverviewExampleDialog,
    UserDialog,
    CustomerDialog,
    WorkFlowDialog,
    InvoiceDialog,
    InboxInvoiceDialog,


    FilterNamesPipe,
    OrderrByPipe
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    MatGridListModule,
    MatCardModule,
    routing,

    MatIconModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatSortModule,
    MatTableModule,
    MatDialogModule,
    MatTooltipModule,
    MatPaginatorModule,
    MatInputModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatNativeDateModule,
    MatDatepickerModule,

    CdkTableModule,

  ],
  providers: [
    LoginGuard,
    AdminGuard,
    AdminOnlyGuard,
    SuperAdminGuard,
    AuthGuard,
    AlertService,
    AuthenticationService,
    UserService,
    AdminService,
    ConfigService,
    WorkFlowService,
    InvoicesService,
  ],
  entryComponents: [DialogOverviewExampleDialog,UserDialog,CustomerDialog,WorkFlowDialog,InvoiceDialog,InboxInvoiceDialog],
  bootstrap: [AppComponent]
})
export class AppModule { }
