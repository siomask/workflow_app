﻿import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserService } from '../_services';
import { Config } from '../_helpers';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        
        if (localStorage.getItem('currentUser')) {
            // logged in so return true
            return true;
        }

        // not logged in so redirect to login page with the return url
        this.router.navigate(['/login'], { });
        return false;
    }
}
@Injectable()
export class LoginGuard implements CanActivate {

    constructor(private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (!localStorage.getItem('currentUser')) {
            return true;
        }

        // not logged in so redirect to login page with the return url
        this.router.navigate(['/'], { });
        return false;
    }
}
@Injectable()
export class AdminGuard implements CanActivate {

    constructor(private router: Router,private userServ: UserService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if ( this.userServ.isAdmin()) {
            return true;
        }

        // not logged in so redirect to login page with the return url
        this.router.navigate([''], { });
        return false;
    }
    
}
@Injectable()
export class AdminOnlyGuard implements CanActivate {

    constructor(private router: Router,private userServ: UserService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if ( this.userServ.isAdminOnly()) {
            return true;
        }

        // not logged in so redirect to login page with the return url
        this.router.navigate([''], { });
        return false;
    }
    
}
@Injectable()
export class SuperAdminGuard implements CanActivate {

    constructor(private router: Router,private userServ: UserService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if ( this.userServ.isSuperAdmin()) {
            return true;
        }

        // not logged in so redirect to login page with the return url
        this.router.navigate([''], { });
        return false;
    }
    
}