﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AlertService, UserService,ConfigService } from '../_services/index';

@Component({
    moduleId: module.id,
    templateUrl: 'register.component.html'
})

export class RegisterComponent {
    model: any = {};
    loading = false;

    constructor(
        private router: Router,
        private userService: UserService,
        private alertService: AlertService,
        private confService: ConfigService) { }

    register() {
        this.loading = true;
        this.userService.create(this.model)
            .subscribe(
                data => {
                    
                    if(data.status){
                        this.alertService.success('Registration successful', true);
                        this.router.navigate(['/profile']);
                    }else{
                        this.alertService.error('Registration unsuccessful, '+data.message);
                    }
                    this.loading = false;
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
}
