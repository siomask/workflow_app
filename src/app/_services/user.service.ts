﻿import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions, Response} from '@angular/http';
import {AuthenticationService} from './authentication.service';
import {AlertService} from './alert.service';
import {User} from '../_models/index';
import {Config} from '../_helpers';
import {ConfigService} from "./config";

@Injectable()
export class USER_SERV {
    protected REMOTE_API:string = '/api/user';
    USER:any = {};
    customers:any = [];
    countries:any = [];
    users:any = [];

    constructor(protected http:Http, protected auth:AuthenticationService, protected alertService:AlertService) {
    }

    protected checkToken(_r:any, flag:boolean = false) {
        if (_r.message.match("Expired") || _r.message == "Not authenticated" || _r.unlogout) {
            this.auth.logout().subscribe((data)=> {
                console.log(data);
            });
        } else if (!_r.status) {
            this.alertService.error(_r.message);
        } else {
            if (!flag)this.alertService.success(_r.message);
        }
        return true;
    }


    protected jwt() {
        // create authorization header with jwt token
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser && currentUser.token) {
            let headers = new Headers({'authorization': currentUser.token});
            return new RequestOptions({headers: headers});
        }
    }
}

@Injectable()
export class UserService extends USER_SERV {

    constructor(protected http:Http, protected auth:AuthenticationService, protected alertService:AlertService, protected confService:ConfigService) {
        super(http, auth, alertService);
    }

    isAdmin() {
        return this.USER && (this.USER.role == this.confService.USER_ROLE.ADMIN || this.USER.role == this.confService.USER_ROLE.SUPER);
    }

    isAdminOnly() {
        return this.USER && (this.USER.role == this.confService.USER_ROLE.ADMIN);
    }

    isSuperAdmin() {
        return this.USER && this.USER.role == this.confService.USER_ROLE.SUPER;
    }

    user(next) {
        if (this.USER) {
            next(this.USER);
        } else {
            this.getOne().subscribe((res)=> {
                next(this.USER);
            })
        }
    }

    getOne() {
        return this.http.get(this.REMOTE_API, this.jwt()).map((response:Response) => {
            let _r = response.json();
            this.checkToken(_r, true);
            this.USER = _r.data || {};
            if (_r.status){
                // this.USER.users = _r.counts - 1;
                this.http.get('api/countries', this.jwt()).map((res)=> {
                    let _c = res.json();
                    if (_c.status)this.countries = _c.data;
                }).subscribe((e)=>{
                    console.log(e);
                });
            }
           
            return this.USER;
        });
    }

    create(user:User) {
        return this.http.post('auth/register', user, this.jwt()).map((response:Response) => {
            let _r = response.json();
            if (_r && _r.token) {
                localStorage.setItem('currentUser', JSON.stringify(_r));
            }
            return _r;
        });
    }

    update(user:User) {
        return this.http.put(this.REMOTE_API, user, this.jwt()).map((response:Response) => {
            let _r = response.json();
            this.checkToken(_r);
            return _r;
        });
    }

    delete(id:number) {
        return this.http.delete(this.REMOTE_API + id, this.jwt()).map((response:Response) => response.json());
    }

}
@Injectable()
export class AdminService extends USER_SERV {

    CUSTOMER_API:string = '/api/customer';
    protected ADMIN_API:string = '/api/admin/users';
    protected SUPER_ADMIN_API:string = '/api/admin/super';

    constructor(protected http:Http, protected auth:AuthenticationService, protected alertService:AlertService) {
        super(http, auth, alertService);
    }

    create(user:User) {
        return this.req(user);

    }

    createCustomer(data:any) {
        return this.http.post(this.CUSTOMER_API, data, this.jwt()).map((response:Response) => {
            let _r = response.json();
            this.checkToken(_r);
            return _r;
        });

    }

    getAll(data) {
        return this.http.get(this.ADMIN_API + (data ? "?role=" + data.role : ''), this.jwt()).map((response:Response) => {
            let _r = response.json();
            // this.checkToken(_r);
            return _r;
        });
    }

    getAllCustomer(data) {
        return this.http.get(this.CUSTOMER_API, this.jwt()).map((response:Response) => {
            let _r = response.json();
            return _r;
        });
    }

    update(user:any) {
        return this.http.put(this.ADMIN_API, user, this.jwt()).map((response:Response) => {
            let _r = response.json();
            this.checkToken(_r);
            return _r;
        });
    }

    updateCustomer(user:any) {
        return this.http.put(this.CUSTOMER_API, user, this.jwt()).map((response:Response) => {
            let _r = response.json();
            this.checkToken(_r);
            return _r;
        });
    }

    request(url, data:any = {}) {
        return this.http.post(this.SUPER_ADMIN_API + "/" + url, data, this.jwt()).map((response:Response) => {
            return response.json();
        });
    }

    req(data:any = {}, url:string = null) {
        return this.http.post(this.ADMIN_API + (url ? "/" + url : ""), data, this.jwt()).map((response:Response) => {
            let _r = response.json();
            if (url != 'activity')this.checkToken(_r);
            return _r;
        });
    }

    delete(id:number) {
        return this.http.delete(this.ADMIN_API + "/" + id, this.jwt()).map((response:Response) => {
            let _r = response.json();
            this.checkToken(_r);
            return _r;
        });
    }

    deleteCustomer(id:number) {
        return this.http.delete(this.CUSTOMER_API + "/" + id, this.jwt()).map((response:Response) => {
            let _r = response.json();
            this.checkToken(_r);
            return _r;
        });
    }

}
@Injectable()
export class WorkFlowService extends AdminService {
    constructor(protected http:Http, protected auth:AuthenticationService, protected alertService:AlertService) {
        super(http, auth, alertService);
        this.SUPER_ADMIN_API += '/workflow'
    }

    get() {
        return this.http.get(this.SUPER_ADMIN_API, this.jwt()).map((response:Response) => {
            let _r = response.json();
            this.checkToken(_r, true);
            return _r;
        });
    }

    getUsers() {
        return this.http.get(this.SUPER_ADMIN_API + "/users", this.jwt()).map((response:Response) => {
            let _r = response.json();
            this.checkToken(_r, true);
            return _r;
        });
    }

    create(data:any = {}, url:string = null) {
        return this.http.post(this.SUPER_ADMIN_API + (url ? "/" + url : ""), data, this.jwt()).map((response:Response) => {
            let _r = response.json();
            this.checkToken(_r);
            return _r;
        });
    }

    update(data:any = {}, url:string = null) {
        return this.http.put(this.SUPER_ADMIN_API + (url ? "/" + url : ""), data, this.jwt()).map((response:Response) => {
            let _r = response.json();
            this.checkToken(_r);
            return _r;
        });
    }

    delete(data:any = {}, url:string = null) {
        return this.http.post(this.SUPER_ADMIN_API + "/delete", data, this.jwt()).map((response:Response) => {
            let _r = response.json();
            this.checkToken(_r);
            return _r;
        });
    }

}

@Injectable()
export class InvoicesService extends AdminService {
    constructor(protected http:Http, protected auth:AuthenticationService, protected alertService:AlertService) {
        super(http, auth, alertService);
        this.SUPER_ADMIN_API += '/invoice'
    }

    get(id:any = null) {
        return this.http.get(this.ADMIN_API + "/invoices" + (id ? "/" + id : ''), this.jwt()).map((response:Response) => {
            let _r = response.json();
            this.checkToken(_r, true);
            return _r;
        });
    }

    approve(data) {
        return this.http.post(this.ADMIN_API + "/invoices", data, this.jwt()).map((response:Response) => {
            let _r = response.json();
            this.checkToken(_r);
            return _r;
        });
    }

    create(data:any = {}, url:string = null) {
        return this.http.post(this.SUPER_ADMIN_API + (url ? "/" + url : ""), data, this.jwt()).map((response:Response) => {
            let _r = response.json();
            this.checkToken(_r);
            return _r;
        });
    }

    update(data:any = {}, url:string = null) {
        return this.http.put(this.SUPER_ADMIN_API + (url ? "/" + url : ""), data, this.jwt()).map((response:Response) => {
            let _r = response.json();
            this.checkToken(_r);
            return _r;
        });
    }

    delete(data:any = {}, url:string = null) {
        return this.http.post(this.SUPER_ADMIN_API + "/delete", data, this.jwt()).map((response:Response) => {
            let _r = response.json();
            this.checkToken(_r);
            return _r;
        });
    }

}