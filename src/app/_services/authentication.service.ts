﻿import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import {  Router } from '@angular/router';

@Injectable()
export class AuthenticationService {
    constructor(private http: Http,  private router: Router) { }

    login(username: string, password: string) {
        return this.http.post('/auth/login',  ({ email: username, password: password }))
            .map((response: Response) => {
                // login successful if there's a jwt token in the response
                let user = response.json();
                if (user && user.token) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                }

                return user;
            });
    }

    resetPsw(username){
        return this.http.post('/auth/resetPsw',  ({ email: username}))
            .map((response: Response) => {
                return response.json();
            });
    }
    
    logout() {
        // remove user from local storage to log user out
        return this.http.post('/auth/logout',  ({}))
            .map((response: Response) => {
                // login successful if there's a jwt token in the response
                let user = response.json();
                if (user.status ||user.unlogout) {
                    localStorage.removeItem('currentUser');
                    this.router.navigate(['/login']);
                    
                }
                 
                return user;
            });
        
    }
}