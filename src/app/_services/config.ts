import {Injectable,OnInit} from '@angular/core';
import 'rxjs/add/operator/map'

@Injectable()
export class ConfigService implements OnInit  {
    PATTERNS:any = {
        URL: /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi,
        EMAIL: /\S+@\S+\.\S+/
    };
      USER_ROLE:any={
        SUPER:1,
        ADMIN:2,
        CUSTOMER:3,
    }
      USER_STATUS:any={
        ACTIVE:2,
        BAN:1,
    }
    ACTIVITY:any=[
        "",
        "have been logged in",
        "have been logged out",
        "have been create the user ",
        "have been update the user ",
        "have been delete the user ",
        "have been create customer ",
        "have been update customer ",
        "have been delete customer ",
        "have been send messages",
        "have been reset password",
    ]
    
    constructor() {
    }
    ngOnInit(){
         
    }

}