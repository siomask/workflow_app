﻿import {Routes, RouterModule} from '@angular/router';

import {HomeComponent} from './home/index';
import {ProfileComponent} from './home/profile/index';
import {UserListComponent} from './home/users/index';
import {LoginComponent} from './login/index';
import {AuthGuard, LoginGuard,AdminGuard} from './_guards/index';
import {CustomerListComponent} from "./home/customers/index";
import {MailComponent} from "./home/mail/index";
import {DefaultComponent} from "./home/default/index";
import {SuperAdminGuard, AdminOnlyGuard} from "./_guards/auth.guard";
import {ActivityComponent} from "./home/activity/index";
import {WorkFlowComponent} from "./home/workflow/index";
import {InvoiceComponent} from "./home/invoices/index";
import {InboxComponent} from "./home/inbox/index";
import {ForgotComponent} from "./resetpsw/index";

const appRoutes:Routes = [
    {
        path: '', component: HomeComponent, canActivate: [AuthGuard],
        children: [
            {path: '', component: DefaultComponent},
            {path: 'profile', component: ProfileComponent},
            {path: 'mail', component: MailComponent, canActivate: [AdminGuard]},
            {path: 'workflow', component: WorkFlowComponent, canActivate: [SuperAdminGuard]},
            {path: 'invoices', component: InvoiceComponent, canActivate: [AdminGuard]},
            {path: 'inbox', component: InboxComponent, canActivate: [AdminOnlyGuard]},
            {path: 'users', component: UserListComponent, canActivate: [SuperAdminGuard]},
            {path: 'activity', component: ActivityComponent, canActivate: [AdminGuard]},
            {path: 'customers', component: CustomerListComponent, canActivate: [AdminGuard]}
        ]
    },

    {path: 'login', component: LoginComponent, canActivate: [LoginGuard]},
    {path: 'forgotpsw', component: ForgotComponent, canActivate: [LoginGuard]},
    // {path: 'register', component: RegisterComponent, canActivate: [LoginGuard]},

    // otherwise redirect to home
    {path: '**', redirectTo: ''}
];

export const routing = RouterModule.forRoot(appRoutes);