﻿import {Component, Inject,ViewChild} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {FormGroup,FormBuilder,FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {UserService} from "../../_services/index";


@Component({
    selector: 'dialog-overview-invoice-dialog',
    templateUrl: 'index.html',
    styleUrls: ['index.scss']
})
export class InvoiceDialog {
    private loading: boolean=false;
    private invoiceForm: FormGroup;
    private elems:any = {
        countries:[]
    };
    @ViewChild("formSubmit")
    formSubmit:any;
    constructor(
        private userService: UserService,
        private formBuilder: FormBuilder,
        public dialogRef: MatDialogRef<InvoiceDialog>,
        @Inject(MAT_DIALOG_DATA) public data: any) {

        this.invoiceForm = this.formBuilder.group({
            item:new FormControl('', [ ]),
            invoiceFrom:new FormControl('', [ Validators.required ]),
            description:new FormControl('', [ ]),
            billDueDate:new FormControl('', [ ]),
            amountDue:new FormControl('', [Validators.required ]),
            amountPaid:new FormControl('', [ ]),
            billToClientCity:new FormControl('', [ ]),
            billToClientState:new FormControl('', [ ]),
            billToClientCountry:new FormControl('', [ ]),
            billToClientAdress:new FormControl('', [ ]),
            billToClientName:new FormControl('', [ Validators.required]),
            billToClientPhone:new FormControl('', [ ]),
            billToClientZipCode:new FormControl('', [ ]),
            billToClientEmail:new FormControl('', [ Validators.required,Validators.email]),
        });
        this.elems.countries=this.userService.countries;
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    onSuccess(){
        this.formSubmit['nativeElement'].click();
        this.invoiceForm['submitted'] = true;
        if(!this.invoiceForm.valid)return;
        this.loading = true;
        this.data.onSuccess(this.data.model,()=>{
            this.loading = false;
            this.dialogRef.close();
        });
    }


}