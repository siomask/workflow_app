﻿import {Component, Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
    selector: 'dialog-overview-example-dialog',
    templateUrl: 'index.html',
})
export class DialogOverviewExampleDialog {

    isOk:boolean=false;
    constructor(
        public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
        @Inject(MAT_DIALOG_DATA) public data: any) { }

    onNoClick(): void {
        this.isOk = false;
        this.dialogRef.close();
    }
    

}