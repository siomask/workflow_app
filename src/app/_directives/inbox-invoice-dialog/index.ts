﻿import {Component, Inject, ViewChild} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {FormGroup, FormBuilder, FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';

declare var prompt:any;
@Component({
    selector: 'dialog-overview-inbox-invoice-dialog',
    templateUrl: 'index.html',
    styleUrls: ['index.scss']
})
export class InboxInvoiceDialog {
    private loading:boolean = false;
    private invoiceForm:FormGroup;
    private invoiceFormT:boolean;
    @ViewChild("formSubmit")
    formSubmit:any;

    constructor(private formBuilder:FormBuilder,
                public dialogRef:MatDialogRef<InboxInvoiceDialog>,
                @Inject(MAT_DIALOG_DATA) public data:any) {
        let _m = data.model, _res = '';
        ['billToClientAdress', 'billToClientCity', 'billToClientState', 'billToClientCountry', 'billToClientZipCode'].forEach((el)=> {
            _res += (_m[el] ? _m[el] + "," : "");
        });
        if (_res.length)_res = _res.substr(0, _res.length - 1);
        data._adress = _res;
        this.invoiceForm = this.formBuilder.group({
            message: new FormControl('', [])
        });

    }

    onNoClick():void {
        this.dialogRef.close();
    }

    private action(act) {
        this.loading = true;
        this.data.onSuccess({status: act, id: this.data.model.id, message: this.data.model.message}, ()=> {
            this.loading = false;
            this.dialogRef.close();
        });
    }

    onSuccess(act) {
        this.action(1);
    }

    onReject(type) {
        if (!this.invoiceFormT) {
            this.invoiceForm = this.formBuilder.group({
                message: new FormControl(this.data.model.message, [Validators.required])
            });
            this.invoiceFormT = true;
        }
        if (this.invoiceForm.valid) {
            this.action(type);
        }
    }
}