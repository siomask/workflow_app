/**
 * Created by Админ on 19.11.2017.
 */
export class Config {
    static PATTERNS:any = {
        URL: /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi,
        EMAIL: /\S+@\S+\.\S+/
    };
    static USER_ROLE:any={
        SUPER:1
    }
    static USER_STATUS:any={
        ACTIVE:2,
        BAN:1,
    }
}
