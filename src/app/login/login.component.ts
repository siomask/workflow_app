﻿import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {FormGroup,FormBuilder,FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import {AlertService, AuthenticationService, ConfigService} from '../_services/index';

export class MyErrorStateMatcher implements ErrorStateMatcher {
    isErrorState(control:FormControl | null, form:FormGroupDirective | NgForm | null):boolean {
        const isSubmitted = form && form.submitted;
        return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    }
}

@Component({
    moduleId: module.id,
    templateUrl: 'login.component.html',
    styleUrls: ['index.scss']
})

export class LoginComponent implements OnInit {
    private model:any = {};
    private loading = false;
    private forgot = false;
    private returnUrl:string;
    private clicked:boolean= false;
    private hidePsw:boolean= true;
    private loginForm: FormGroup;
    private forgotForm: FormGroup;
    private matcher = new MyErrorStateMatcher();

    constructor(private route:ActivatedRoute,
                private router:Router,
                private authenticationService:AuthenticationService,
                private alertService:AlertService,
                private confService:ConfigService,
                private formBuilder: FormBuilder
    ) {
        this.loginForm = this.formBuilder.group({
            email:    new FormControl('', [
                Validators.required,
                Validators.email,
            ]),
            psw:new FormControl('', [
                Validators.required
            ])
        });
        this.forgotForm = this.formBuilder.group({
            email:    new FormControl('', [
                Validators.required,
                Validators.email,
            ])
        });
    }

    ngOnInit() {
        // reset login status
        this.authenticationService.logout();

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    private _forgot() {
        this.forgot = !this.forgot;
        this.model={};
    }
    private login() {
        if(!this.loginForm.valid)return;
        this.loading =  true;
        this.authenticationService.login(this.model.email, this.model.password)
            .subscribe(
                data => {
                    if (data.status) {
                        this.router.navigate(['']);
                    } else {
                        this.alertService.error('Registration unsuccessful,' + data.message);
                    }
                    this.loading = false;
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }

    private resetPsw() {
        if(!this.forgotForm.valid)return;
        this.loading = true;
        this.authenticationService.resetPsw(this.model.email)
            .subscribe(
                data => {
                    if (data.status) {
                        this.alertService.success(data.message);
                        this.forgot = false;
                    } else {
                        this.alertService.error('reset password unsuccessful,' + data.message);
                    }
                    this.loading = false;
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }

}
